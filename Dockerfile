FROM python
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY python.py /python.py
CMD python /python.py