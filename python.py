from flask import Flask, request
import redis
import time
app = Flask(__name__)

redis_data = redis.Redis(host='redis', port=6379)

@app.route("/", methods=['POST'])
def store_data():
    request.get_data()
    data = request.data
    redis_data.set(time.time(), str(data))
    return data

@app.route("/", methods=['GET'])
def get_data():
    values = []
    for key in redis_data.keys():
      values.append(redis_data.get(key).decode("utf-8"))
    return str(values)

if __name__ == "__main__":
    app.run(host='0.0.0.0')